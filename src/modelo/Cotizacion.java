package modelo;

import java.util.ArrayList;

public class Cotizacion {
    private ArrayList<Cotizacion> cotizaciones;
 
    private String descripcionAutomovil;
    private int numCotizacion;   
    private int plazo;
    private double precio;
    private double porcentajePagoInicial;
    private double pagoInicial;

    public ArrayList<Cotizacion> getCotizaciones() {
        return cotizaciones;
    }

    public void setCotizaciones(ArrayList<Cotizacion> cotizaciones) {
        this.cotizaciones = cotizaciones;
    }

    public int getNumCotizacion() {
        return numCotizacion;
    }

    public void setNumCotizacion(int numCotizacion) {
        this.numCotizacion = numCotizacion;
    }

    public String getDescripcionAutomovil() {
        return descripcionAutomovil;
    }

    public void setDescripcionAutomovil(String descripcionAutomovil) {
        this.descripcionAutomovil = descripcionAutomovil;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getPorcentajePagoInicial() {
        return porcentajePagoInicial;
    }

    public void setPorcentajePagoInicial(double porcentajePagoInicial) {
        this.porcentajePagoInicial = porcentajePagoInicial;
    }

    public double getPagoInicial() {
        return pagoInicial;
    }

    public void setPagoInicial(double pagoInicial) {
        this.pagoInicial = pagoInicial;
    }
    
     public double Cpagoinicial() {
        return precio * porcentajePagoInicial / 100;
    }

    public double Totalfin() {
        return precio - Cpagoinicial();
    }

    public double PagoMensual() {
        return Totalfin() / plazo;
    }
    
}


